#/bin/bash
#
#
USB=$(lsusb | grep Google)
echo $USB
if [[ -z $USB ]]; then
  echo "Device not found on USB. Are you seeing it as being connected?"
  exit 2;
fi
apt update
apt install wget unzip -y
wget https://dl.google.com/android/repository/platform-tools-latest-linux.zip
unzip $(ls | grep platform)
wget https://dl.google.com/dl/android/aosp/sunfish-rq3a.211001.001-factory-5d6da39f.zip
unzip sunfish-rq3a.211001.001-factory-5d6da39f.zip
cd sunfish-rq3a.211001.001/
../platform-tools/fastboot --version
sleep 10
../platform-tools/fastboot flash bootloader bootloader-sunfish-s5-0.3-7357976.img
../platform-tools/fastboot reboot-bootloader
sleep 5
../platform-tools/fastboot flash radio radio-sunfish-g7150-00047-210319-b-7220401.img
../platform-tools/fastboot reboot-bootloader
sleep 5
../platform-tools/fastboot -w update image-sunfish-rq3a.211001.001.zip --skip-reboot
sleep 15
echo "Device Provisioned. Add the..."
echo "carbonite.nowsecure.io/provision-needed: \"\" " 
echo "...annotation to the SoloDevice entry in Cluser Explorer or just provision the device through the Carbonite UI is it's listed there."
#../platform-tools/fastboot devices
